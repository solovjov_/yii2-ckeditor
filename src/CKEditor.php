<?php

namespace solovyevlv\ckeditor;

use yii\helpers\Json;
use yii\widgets\InputWidget;
use yii\helpers\Html;
use solovyevlv\ckeditor\interfaces\IPackage;

class CKEditor extends InputWidget implements IPackage
{
    public $jsFile = 'ckeditor.js';

    public $url = 'https://cdn.ckeditor.com';

    public $version = '4.7.2';

    public $package = 'full';

    public $params = [];

    public function init()
    {
        parent::init();

        $url = $this->url .'/'.$this->version.'/'.$this->package.'/'.$this->jsFile;

        $this->view = \Yii::$app->getView();

        $this->view->registerJsFile($url);
    }

    public function run()
    {
        $options = array_merge($this->field->inputOptions, $this->options);

        echo Html::activeTextarea($this->model, $this->field->attribute, $options);

        $params = $this->prepareParams();

        $this->view->registerJs("CKEDITOR.replace('{$options['id']}',{$params});");

    }

    private function prepareParams()
    {
        return Json::encode($this->params);
    }
}
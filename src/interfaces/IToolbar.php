<?php

namespace solovyevlv\ckeditor\interfaces;

interface IToolbar
{
    const ITEM_DELIMETER = '-';

    const ITEM_BOLD = 'Bold';

    const ITEM_ITALIC = 'Italic';

    const ITEM_UNDERLINE = 'Underline';

    const ITEM_FORMAT = 'Format';

    const ITEM_SOURCE = 'Source';

    const ITEM_PREVIEW = 'Preview';

    const ITEM_CUT = 'Cut';

    const ITEM_COPY = 'Copy';

    const ITEM_PASTE = 'Paste';

    const ITEM_FONT = 'Font';

    const ITEM_FONT_SIZE = 'FontSize';

    const ITEM_TEXT_COLOR = 'TextColor';

    const ITEM_IMAGE = 'Image';

    const ITEM_FORM = 'Form';

    const ITEM_UNDO = 'Undo';

    const ITEM_REDO = 'Redo';

    const ITEM_FIND = 'Find';

    const ITEM_LINK = 'Link';

    const ITEM_UNLINK = 'Unlink';

    const ITEM_ANCHOR = 'Anchor';

    const ITEM_NUMBERED_LIST = 'NumberedList';

    const ITEM_BULLETED_LIST = 'BulletedList';

    const ITEM_MAXIMIZE = 'Maximize';

    const ITEM_TABLE = 'Table';

    const ITEM_JUSTIFY_LEFT = 'JustifyLeft';

    const ITEM_JUSTIFY_CENTER = 'JustifyCenter';

    const ITEM_JUSTIFY_RIGHT = 'JustifyRight';

    const ITEM_JUSTIFY_BLOCK = 'JustifyBlock';

    const ITEM_HORIZONTAL_RULE = 'HorizontalRule';

    const ITEM_BLOCKQUOTE = 'Blockquote';

    const ITEM_SCAYT = 'Scayt'; //Проверка орфографии
    //TODO: 'Outdent', 'Indent', 'ShowBlocks'
}
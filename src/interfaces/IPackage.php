<?php

namespace solovyevlv\ckeditor\interfaces;

interface IPackage
{
    const PACKAGE_BASIC = 'basic';

    const PACKAGE_STANDARD = 'standard';

    const PACKAGE_FULL = 'full';
}
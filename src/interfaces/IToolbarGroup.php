<?php

namespace solovyevlv\ckeditor\interfaces;

interface IToolbarGroup
{
    const GROUP_BASIC_STYLES = 'basicstyles';

    const GROUP_CLEANUP = 'cleanup';

    const GROUP_STYLES = 'styles';

    const GROUP_COLORS = 'colors';

    const GROUP_SOURCE = 'mode';

    const GROUP_CLIPBOARD = 'clipboard';

    const GROUP_UNDO = 'undo';

    const GROUP_DOCUMENT = 'document';

    const GROUP_LINKS = 'links';

    const GROUP_INSERT = 'insert';

    const GROUP_TOOLS = 'tools';

    const GROUP_LIST = 'list';

    //TODO: 'indent','blocks','cleanup'

}
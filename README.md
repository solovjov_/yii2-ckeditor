CKEditor 
==========================
Виджет для Yii2 приложений.

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist solovyevlv/yii2-ckeditor "1.0"
```

or add

```
"solovyevlv/yii2-ckeditor": "1.0"
```

Добавить в composer.json в секцию repositories

```
"repositories" : [
        {
            "type" : "git",
            "url" : "https://solovjov_@bitbucket.org/solovjov_/yii2-ckeditor.git"
        }
    ],

```


Примеры использования (по умолчанию подключен пакет - full)
-----

```
<?php

use solovyevlv\ckeditor\CKEditor;

$form->field($model, 'text')->widget(CKEditor::className(), [
            'options' => ['rows' => 6],
        ]);?>
```

Примеры использования с определенным пакетом
-----

```
<?php

use solovyevlv\ckeditor\CKEditor;

$form->field($model, 'text')->widget(CKEditor::className(), [
            'options' => ['rows' => 6],
            'package'=> CKEditor::PACKAGE_STANDARD
        ]);?>
```

Гибкая настройка тулбара item-ами
-----

```
<?php

use solovyevlv\ckeditor\CKEditor;
use solovyevlv\ckeditor\interfaces\IToolbar;

$form->field($model, 'text')->widget(CKEditor::className(), [
            'options' => ['rows' => 6],
            'params' => [
				'toolbar' => [
					IToolbar::ITEM_IMAGE,
					IToolbar::ITEM_FORMAT,
					....
				]
			]
        ]);?>
```

Настройка тулбара группами item-ов
-----

```
<?php

use solovyevlv\ckeditor\CKEditor;
use solovyevlv\ckeditor\interfaces\IToolbarGroup;

$form->field($model, 'text')->widget(CKEditor::className(), [
            'options' => ['rows' => 6],
            'params' => [
				'toolbarGroups' => [
					[
						'groups' => [
							IToolbarGroup::GROUP_INSERT,
							IToolbarGroup::GROUP_SOURCE,
							....
						]
					],
					'/', // новая строка
					[
						'name' => 'name_my_group' // не обязательно
						'groups' => [
							IToolbarGroup::GROUP_UNDO,
							.....
						]
					]
				]
			]
        ]);?>
```